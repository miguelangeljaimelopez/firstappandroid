package com.example.miguel.practica1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by miguel on 20/10/16.
 */

public class usuarioSQLiteHelper extends SQLiteOpenHelper{

    String sql = "CREATE TABLE Usuarios (ID INTEGER PRIMARY KEY AUTOINCREMENT,Nombres TEXT, Apellidos TEXT )";

    public usuarioSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST Usuarios");
        db.execSQL(sql);
    }
}
