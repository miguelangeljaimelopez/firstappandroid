package com.example.miguel.practica1;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText id;
    EditText nombres;
    EditText apellidos;
    EditText busqueda;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        id = (EditText)findViewById(R.id.edtID);
        nombres = (EditText) findViewById(R.id.edtNombre);
        apellidos = (EditText) findViewById(R.id.edtApellidos);
        busqueda = (EditText)findViewById(R.id.edtBusqueda);
    }

    public void insertar(View view){

        usuarioSQLiteHelper usuarios = new usuarioSQLiteHelper(this,"DBUsuarios",null,1);
        SQLiteDatabase db = usuarios.getWritableDatabase();

        String myid = id.getText().toString();
        String mynombre = nombres.getText().toString();
        String myapellidos = apellidos.getText().toString();

        if(mynombre.length()>0 && myapellidos.length()>0) {
            db.execSQL("INSERT INTO Usuarios (ID,Nombres,Apellidos) VALUES("+null+",'"+mynombre+"','"+myapellidos+"')");
            db.close();
            limpiar(view);
            Toast.makeText(this,"Insertados correctamente",Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(this,"Ocupas llenar los campos",Toast.LENGTH_SHORT).show();
        }
    }

    public void buscar (View view){
        usuarioSQLiteHelper usuarios = new usuarioSQLiteHelper(this,"DBUsuarios",null,1);
        SQLiteDatabase db = usuarios.getWritableDatabase();

        String mySearch = busqueda.getText().toString();
        if(mySearch.length()>0){
            String[] args = new String []{mySearch};
            Cursor resultado = db.rawQuery("SELECT ID,Nombres,Apellidos FROM Usuarios WHERE Nombres=?",args);

            if(resultado.moveToFirst()){
               id.setText(resultado.getString(0));
                nombres.setText(resultado.getString(1));
                apellidos.setText(resultado.getString(2));
                Toast.makeText(this,"encontrado",Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(this,"no hay registro",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void eliminar(View view){
        usuarioSQLiteHelper usuarios = new usuarioSQLiteHelper(this,"DBUsuarios",null,1);
        SQLiteDatabase db = usuarios.getWritableDatabase();

        String myid = id.getText().toString();

        if(myid.length()>0){
            String[] args = new String []{myid};
            db.execSQL("DELETE FROM Usuarios WHERE ID=?",args);
            Toast.makeText(this,"Eliminado correctamente",Toast.LENGTH_SHORT).show();
            limpiar(view);
        }
        else{
            Toast.makeText(this,"Ocupo un id",Toast.LENGTH_SHORT).show();
        }
    }
    public void limpiar(View view){
        id.setText("");
        nombres.setText("");
        apellidos.setText("");
        busqueda.setText("");
    }
}